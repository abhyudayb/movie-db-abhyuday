import React, { Component } from 'react';
import noImg from '../images/noImg.svg'

class ExtendedResult extends Component{
    render(){
        if(this.props.extendedResult[this.props.index].enabled == 1){
            return (
                <div className="ExtendedResult">
                    <hr />
                    <table>
                        <tbody>
                            <tr>
                                <td><span className="Bold">Genre:</span></td>
                                <td>{this.props.data.Genre}</td>
                            </tr>
                            <tr>
                                <td><span className="Bold">Director:</span></td>
                                <td>{this.props.data.Director}</td>
                            </tr>
                            <tr>
                                <td><span className="Bold">Cast:</span></td>
                                <td>{this.props.data.Actors}</td>
                            </tr>
                            <tr>
                                <td><span className="Bold">Plot:</span></td>
                                <td>{this.props.data.Plot}</td>
                            </tr>
                            <tr>
                                <td><span className="Bold">IMDB Rating:</span></td>
                                <td>{this.props.data.imdbRating}</td>
                            </tr>  
                        </tbody>  
                    </table>
                </div>
            );
        }
        return null;
        /*
        {"Title":"Jumanji: Welcome to the Jungle","Year":"2017","Rated":"PG-13",
        "Released":"20 Dec 2017","Runtime":"119 min","Genre":"Action, Adventure, Comedy",
        "Director":"Jake Kasdan","Writer":"Chris McKenna (screenplay by), Erik Sommers 
        (screenplay by), Scott Rosenberg (screenplay by), Jeff Pinkner (screenplay by), 
        Chris McKenna (story by), Chris Van Allsburg (based on the book \"Jumanji\" by)",
        "Actors":"Dwayne Johnson, Kevin Hart, Jack Black, Karen Gillan","Plot":"Four teenagers 
        discover an old video game console and are literally drawn into the game's jungle setting, 
        becoming the adult avatars they choose.","Language":"English","Country":"USA","Awards":"N/A",
        "Poster":"https://images-na.ssl-images-amazon.com/images/M/MV5BMTkyNDQ1MDc5OV5BMl5BanBnXkFtZTgwOTcyNzI2MzI@._V1_SX300.jpg",
        "Ratings":[{"Source":"Internet Movie Database","Value":"7.2/10"},{"Source":"Rotten Tomatoes",
        "Value":"76%"},{"Source":"Metacritic","Value":"58/100"}],"Metascore":"58","imdbRating":"7.2",
        "imdbVotes":"18,237","imdbID":"tt2283362","Type":"movie","DVD":"N/A","BoxOffice":"$245,606,319",
        "Production":"Columbia Pictures","Website":"http://www.jumanjimovie.com/","Response":"True"}


        */


    }
}

export default ExtendedResult