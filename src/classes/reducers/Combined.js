import { combineReducers } from 'redux';

import searchResults from './Results'
import extendedResult from './ExtendedResult'


const combinedReducer = combineReducers({searchResults, extendedResult});

export default combinedReducer;