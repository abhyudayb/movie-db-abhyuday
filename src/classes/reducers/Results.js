const searchResults = (state = null, action) => {
    switch(action.type){
        case 'FETCHRESULTS' :
            return action.responseJson;
        default:
            return state;    
    }
}

export default searchResults;
