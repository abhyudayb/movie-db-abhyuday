const extendedResult = (state = null, action) => {
    switch(action.type){
        case 'FETCHEXTENDED' :
            if(action.flag == 2){
                return [...state.slice(0,action.index),
                    {data: action.responseJson, enabled: 1},
                    ...state.slice(action.index+1,10)
                ]
            }
            else if(action.flag == 0){
                return [...state.slice(0,action.index),
                    {data: state.data, enabled: 0},
                    ...state.slice(action.index+1,10)
                ]
            }
            else if(action.flag == 1){
                return [...state.slice(0,action.index),
                    {data: state.data, enabled: 1},
                    ...state.slice(action.index+1,10)
                ]
            }
        default:
            return state;    
    }
}

export default extendedResult;