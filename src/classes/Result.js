import React, { Component } from 'react';
import noImg from '../images/noImg.svg'
import ExtendedResult from './ExtendedResult';

class Result extends Component{
    
    handleOnClick(imdbID,i,e){
        let url = "http://www.omdbapi.com/?i="+imdbID+"&apikey=3a3c37a3";
        
        if(this.props.extendedResult[i].enabled == 1){
            this.props.fetchExtendedResult(i, url, imdbID, 0);
            e.target.nextElementSibling.childNodes[2].className = "descImg";
            return;
        }
        e.target.nextElementSibling.childNodes[2].className = "descImgExpanded";
        
        // window.alert(imdbID);
        
        if(this.props.extendedResult[i].data == null){
            this.props.fetchExtendedResult(i, url, imdbID, 2);
        }
        else{
            this.props.fetchExtendedResult(i, url, imdbID, 1);
        }
    }



    render(){
        let imgUrl = null;
        if(this.props.data.Poster === 'N/A')
            imgUrl = noImg;
        else	
            imgUrl = this.props.data.Poster;
        return (
            <div className="Result">
            <div className="Result-title" onClick={(e) => this.handleOnClick(this.props.data.imdbID, this.props.index, e)} >{this.props.data.Title}</div>
            <div className="Result-desc">
                <div className="descContents">Year: {this.props.data.Year}</div>
                <div className="descContents">Type: {this.props.data.Type}</div>
                <img src={imgUrl} className="descImg" />
            </div>
            <ExtendedResult {...this.props} data={this.props.extendedResult[this.props.index].data} index={this.props.index} key={this.props.data.imdbID} />
            </div>
        );
    }
}

export default Result