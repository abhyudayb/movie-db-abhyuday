import React, { Component } from 'react';
import '../css/App.css';

import Result from './Result';

class SearchResults extends Component {

  render(){
    if(this.props.searchResults!=null){
      const movies = this.props.searchResults.Search;
      if(movies != undefined){
        console.log("Hey"+movies);
        return <div className="Search-results" >{movies.map((movie,index) => <Result data={movie} {...this.props} index={index} key={movie.imdbID}/>)}</div>;
      }
    }

    return null;
  }
}

export default SearchResults