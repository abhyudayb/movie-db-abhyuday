import React, { Component } from 'react';

class SearchBar extends Component {  
    
  constructor(props){
    super(props);
    var timeoutVar = null;
  }

  handleKeyDown(){
    clearTimeout(this.timeoutVar);
    this.timeoutVar = setTimeout(this.props.fetchResults.bind(this,document.getElementsByClassName("Search-bar")[0].value), 500);
  }
  
  
  render(){
    return (<input type="text" className="Search-bar" placeholder="Type here. . ." onChange={this.handleKeyDown.bind(this)} />);
  }
}

export default SearchBar