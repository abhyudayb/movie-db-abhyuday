export const loadResults = (responseJson) => {
    return {
        type: 'FETCHRESULTS',
        responseJson
    }
}

export const fetchResults = (searchText) => {
    return (dispatch) => {
        const url = "http://www.omdbapi.com/?s="+searchText+"&apikey=3a3c37a3&page=1";
        var response = null;
        return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch(loadResults(responseJson));  
        });
        // return response;
    }
}


export const loadExtended = (responseJson,index,flag) => {
    return {
        type: 'FETCHEXTENDED',
        responseJson,
        index,
        flag
    }
}

export const fetchExtendedResult = (index, url, imdbId, flag) => {
    
    if(flag == 2)
    return (dispatch) => {
        var response = null;
        return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          dispatch(loadExtended(responseJson,index,flag));  
        });
        // return response;
    }
    else
    return {
        type: 'FETCHEXTENDED',
        index,
        flag
    }


}