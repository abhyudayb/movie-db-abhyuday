import React, { Component } from 'react';
import logo from '../images/logo.png';
import '../css/App.css';
import SearchBar from './SearchBar'
import SearchResults from './SearchResults'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionObjects from './actions/ActionObjects';


const mapStateToProps = (state) => {
  return{
    searchResults: state.searchResults,
    extendedResult: state.extendedResult,
    timeoutVar: state.timeoutVar
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actionObjects, dispatch);
}


class App extends Component {
  
  render() {
   
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Movie Database</h1>
        </header>
        <SearchBar {...this.props} />  
        <SearchResults {...this.props} />  
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);



































// handleKeyDown(){
//   clearTimeout(this.myTimeoutVar);
//   this.myTimeoutVar = setTimeout(()=>{
//     this.setState(
//       {
//         haveResults: false,
//         data: [],
//         totalResults: 0,
//       }
//     );
//     const inputText = document.getElementsByClassName("Search-bar")[0].value;
//     var url = "http://www.omdbapi.com/?s="+inputText+"&apikey=3a3c37a3&page=1";

//     return fetch(url)
//     .then((response) => response.json())
//     .then((responseJson) => {
//       if(responseJson.totalResults>0)
//       this.setState(
//         {
//           haveResults: true,
//           data: responseJson.Search,
//           totalResults: responseJson.totalResults,
//         }
//       );
//       //window.alert(this.state.data);
//     })
//     // var xmlhttp = new XMLHttpRequest();
//     // xmlhttp.onreadystatechange = () => {
//     //   if (this.readyState == 4 && this.status == 200) {
//     //     var myObj = JSON.parse(this.responseText);
//     //     console.log(myObj);
//     //     window.alert(myObj.totalResults);
//     //     this.setState({haveResults: true,});
//     //   }   
//     // };
//     // xmlhttp.open("GET", url, true);
//     // xmlhttp.send();
//   }, 1000);
//   // if(haveIt)
    
//   // else
//   //   this.setState({haveResults: true,});
// }