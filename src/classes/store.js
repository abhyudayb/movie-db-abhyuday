import { createStore, compose } from 'redux';
import { applyMiddleware } from 'redux'
//import the root reducer
import combinedReducer from './reducers/Combined';

import promiseMiddleware from 'redux-promise-middleware';
import thunk from 'redux-thunk'


const defaultState = {
    searchResults : null,
    extendedResult : [{data: null, enabled: 0},
        {data: null, enabled: 0},{data: null, enabled: 0},{data: null, enabled: 0},
        {data: null, enabled: 0},{data: null, enabled: 0},{data: null, enabled: 0},
        {data: null, enabled: 0},{data: null, enabled: 0},{data: null, enabled: 0}],
};

//Dev tools code
const enhancers = compose(
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(combinedReducer, defaultState, applyMiddleware(thunk));

export default store;